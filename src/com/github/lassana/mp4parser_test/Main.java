package com.github.lassana.mp4parser_test;

import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        final File source = new File("1.mp4");
        final File output = new File("1.mp4." + System.currentTimeMillis() + ".mp4");
        final int start = 24*1000;
        final int end = 37*1000;
        Mp4Cutter2.startTrim(source, output, start, end);
    }
}
